package vn.edu.fpt.hoadx.is1103_finalproject.activity;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import vn.edu.fpt.hoadx.is1103_finalproject.R;
import vn.edu.fpt.hoadx.is1103_finalproject.utils.Utils;

public class MainActivity extends Activity implements View.OnClickListener {

    EditText edtMsg;
    Button btnSend;
    TextView tvMain;
    String intent;
    boolean isGreeting;
    boolean isQuestion;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        intent = "";
        isGreeting = false;
        isQuestion = false;

        // edtMsg = (EditText) findViewById(R.id.edtMsg);
        // btnSend = (Button) findViewById(R.id.btnSend);
        //  tvMain = (TextView) findViewById(R.id.tvMain);

//        tvMain.append("Hi there\n");
//        tvMain.append("I am FPTU Assistant. How can I help you?\n");

        btnSend.setOnClickListener(this);

    }

    private void extractText(final String text) {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    URL witEndpoint = new URL("https://api.wit.ai/message?v=20180709&verbose=true&q=" + text);
                    HttpsURLConnection conn = (HttpsURLConnection) witEndpoint.openConnection();
                    conn.setRequestProperty("Authorization", "Bearer QT7BQQAVI4UBAN262IDEYSQVHBVD44KJ");

                    if (conn.getResponseCode() == 200) {
                        conn.setRequestMethod("GET");
                        InputStream is = new BufferedInputStream(conn.getInputStream());
                        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                        StringBuilder sb = new StringBuilder();

                        String line;
                        try {
                            while ((line = reader.readLine()) != null) {
                                sb.append(line).append('\n');
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        } finally {
                            try {
                                is.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }

                        String jsonString = sb.toString();
                        conn.disconnect();

                        Log.v("DumpData", jsonString);

                        JSONObject root = new JSONObject(jsonString);
                        JSONObject entities = root.getJSONObject("entities");

                        String response = "";
                        if (Utils.extractEntity(entities, "greeting", "true", 0.8) != "") {
                            // greetings
                            if (!isGreeting) {
                                tvMain.setText("Hi there\n");
                                tvMain.append("I am FPTU Assistant. How can I help you?\n");
                            } else {
                                tvMain.append("Hi there\n");
                            }
                            isGreeting = true;
                            return;
                        }
                        if (Utils.extractEntity(entities, "bye", "true", 0.8) != "") {
                            // greetings
                            tvMain.append("Goodbye, see ya.\n");
                            isGreeting = false;
                            return;
                        }
                        if (Utils.extractEntity(entities, "intent", "get_student_info", 0.8) != "") {
                            // user want to get student information
                            intent = "get_student_info";
                            if ((response = Utils.extractEntity(entities, "contact", "", 0.8)) != "") {
                                tvMain.append("You want to get information of student " + response.substring(0, 1).toUpperCase() + response.substring(1) + ", don't you ?\n");
                                isQuestion = true;
                            } else if ((response = Utils.extractEntity(entities, "roll_number", "true", 0.8)) != "") {
                                tvMain.append("You want to get information of student has roll number" + response + ", don't you ?\n");
                                isQuestion = true;
                            } else {
                                tvMain.append("You must enter student name or student roll number\n");
                            }
                            return;
                        }
                        if ((response = Utils.extractEntity(entities, "contact", "", 0.8)) != "") {
                            if (intent.equals("get_student_info")) {
                                tvMain.append("You want to get information of student " + response.substring(0, 1).toUpperCase() + response.substring(1) + ", don't you ?\n");
                                isQuestion = true;
                            } else {
                                tvMain.append("Oops!!! I cannot recognize what do you want. Sorry about that.\n");
                            }
                            return;
                        }
                        if ((response = Utils.extractEntity(entities, "roll_number", "true", 0.8)) != "") {
                            if (intent.equals("get_student_info")) {
                                tvMain.append("You want to get information of student has roll number" + response + ", don't you ?\n");
                                isQuestion = true;
                            } else {
                                tvMain.append("Oops!!! I cannot recognize what do you want. Sorry about that.\n");
                            }
                            return;
                        }

                        if (isQuestion) {
                            if (Utils.extractEntity(entities, "intent", "yes", 0.8) != "") {
                                if (intent.equals("get_student_info")) {
                                    tvMain.append("Wait a minute, I am searching student information...\n");
                                }
                                isQuestion = false;
                                return;
                            }
                            if (Utils.extractEntity(entities, "intent", "no", 0.8) != "") {
                                tvMain.append("Thank you. Do you have any question?\n");
                                isQuestion = false;
                                return;
                            }
                        }
                        tvMain.append("Sorry. What do you want?\n");
                    } else {
                    }

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }


    @Override
    public void onClick(View v) {
        int UI_id = v.getId();
        switch (UI_id) {
            case R.id.btnSend:
                String msg = edtMsg.getText().toString();
                edtMsg.setText("");
                extractText(msg);
        }
    }
}

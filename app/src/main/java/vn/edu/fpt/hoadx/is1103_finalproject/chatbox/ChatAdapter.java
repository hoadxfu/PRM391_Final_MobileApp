package vn.edu.fpt.hoadx.is1103_finalproject.chatbox;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import vn.edu.fpt.hoadx.is1103_finalproject.activity.ChatActivity;
import vn.edu.fpt.hoadx.is1103_finalproject.chatbox.entity.Message;
import vn.edu.fpt.hoadx.is1103_finalproject.R;
import vn.edu.fpt.hoadx.is1103_finalproject.chatbox.messageHolder.ReceivedMessageHolder;
import vn.edu.fpt.hoadx.is1103_finalproject.chatbox.messageHolder.SentMessageHolder;

public class ChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int VIEW_TYPE_MESSAGE_SENT = 1;
    private static final int VIEW_TYPE_MESSAGE_RECEIVED = 2;
    private static final int VIEW_TYPE_MESSAGE_MULTIPLE_CHOICE = 3;
    private static final String MESSAGE_WITH_EVENT = " (Click your choice)";
    private String BOT = "PsyDuck";
    private String urlProfileImage = "https://sv1.uphinhnhanh.com/images/2018/07/17/psyduckBot2.jpg";
    private ChatActivity activity;
    private ChatHistory chatHistory;
    private OnClickListener onClickListener;

    private ArrayList<Message> mMessageList;
    private Message m;

    public ChatAdapter(final ChatActivity activity) {
        this.activity = activity;
        mMessageList = new ArrayList<>();
        chatHistory = new ChatHistory(activity, activity.getUserN());
        loadPreviousMessages();
    }

    // Appends a new message to the beginning of the message list.
    public void appendMessage(String userN, String message) {
        message = message.trim();
        if (message.isEmpty() || message.equals("\n") || message.equals("")) {
            return;
        }
        m = new Message(userN, message, urlProfileImage);

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mMessageList.add(0, m);
                notifyDataSetChanged();
                chatHistory.saveNewMessage(m);
            }
        });
    }

    //Appends a multiple choices
    public void appendMessage(String userN, List<Message> listMes, RecyclerView.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
        for (Message mes : listMes) {
            String message = mes.getMsg();
            message = message.trim();
            message = message + MESSAGE_WITH_EVENT;
            if (message.isEmpty() || message.equals("\n") || message.equals("")) {
                return;
            }
            m = new Message(userN, message, urlProfileImage);

            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mMessageList.add(0, m);
                    notifyDataSetChanged();
                    chatHistory.saveNewMessage(m);
                }
            });
        }
    }

    // Determines the appropriate ViewType according to the sender of the message.
    @Override
    public int getItemViewType(int position) {
        Message message = mMessageList.get(position);

        if (!message.getSender().equals(BOT)) {
            // If the current user is the sender of the message
            return VIEW_TYPE_MESSAGE_SENT;
        } else {
            if (message.getMsg().endsWith(MESSAGE_WITH_EVENT)) {

                //message.setMsg(message.getMsg().substring(0, message.getMsg().length() - MESSAGE_WITH_EVENT.length()));
                return VIEW_TYPE_MESSAGE_MULTIPLE_CHOICE;
            }
            // If some other user sent the message
            return VIEW_TYPE_MESSAGE_RECEIVED;
        }
    }

    // Retrieves 10 most recent messages.
    public void loadPreviousMessages() {
        if (chatHistory.getOldMessages() != null) {
            List<Message> list = chatHistory.getOldMessages().getListMes();
            if (list != null) {
                mMessageList.clear();
                mMessageList.addAll(list);
            }
        } else {
            appendMessage(BOT, "Welcome! I'm PsyDuck. " +
                    "I only know some information of FU students. So, Could I help you?");
        }
        notifyDataSetChanged();
    }

    // Inflates the appropriate layout according to the ViewType.
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;

        if (viewType == VIEW_TYPE_MESSAGE_MULTIPLE_CHOICE) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_message_received, parent, false);
            view.setOnClickListener(onClickListener);
            return new ReceivedMessageHolder(view);
        } else if (viewType == VIEW_TYPE_MESSAGE_SENT) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_message_sent, parent, false);
            return new SentMessageHolder(view);
        } else if (viewType == VIEW_TYPE_MESSAGE_RECEIVED) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_message_received, parent, false);
            return new ReceivedMessageHolder(view);
        }

        return null;
    }

    // Passes the message object to a ViewHolder so that the contents can be bound to UI.
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Message message = mMessageList.get(position);
        switch (holder.getItemViewType()) {
            case VIEW_TYPE_MESSAGE_MULTIPLE_CHOICE:
                ((ReceivedMessageHolder) holder).bind(message, activity);
                break;
            case VIEW_TYPE_MESSAGE_SENT:
                ((SentMessageHolder) holder).bind(message);
                break;
            case VIEW_TYPE_MESSAGE_RECEIVED:
                ((ReceivedMessageHolder) holder).bind(message, activity);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return mMessageList.size();
    }


}


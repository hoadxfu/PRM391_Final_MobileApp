package vn.edu.fpt.hoadx.is1103_finalproject.chatbox.entity;

import java.sql.Timestamp;

public class Message {
    String sender;
    String msg;
    long createdAt;
    String urlProfileImage;

    public Message(String sender, String msg, String urlProfileImage) {
        this.sender = sender;
        this.msg = msg;
        Timestamp ts = new Timestamp(System.currentTimeMillis());
        this.createdAt = ts.getTime();
        this.urlProfileImage = urlProfileImage;
    }

    public String getUrlProfileImage() {
        return urlProfileImage;
    }

    public void setUrlProfileImage(String urlProfileImage) {
        this.urlProfileImage = urlProfileImage;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }
}

package vn.edu.fpt.hoadx.is1103_finalproject.chatbox.messageHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import vn.edu.fpt.hoadx.is1103_finalproject.activity.ChatActivity;
import vn.edu.fpt.hoadx.is1103_finalproject.chatbox.entity.Message;
import vn.edu.fpt.hoadx.is1103_finalproject.R;
import vn.edu.fpt.hoadx.is1103_finalproject.utils.ImageUtils;
import vn.edu.fpt.hoadx.is1103_finalproject.utils.TimeUtils;
import vn.edu.fpt.hoadx.is1103_finalproject.utils.Utils;

// Messages sent by others display a profile image and nickname.
public class ReceivedMessageHolder extends RecyclerView.ViewHolder {
    TextView messageText, timeText, nameText;
    ImageView profileImage;

    public ReceivedMessageHolder(View itemView) {
        super(itemView);
        messageText = itemView.findViewById(R.id.text_message_body);
        timeText = itemView.findViewById(R.id.text_message_time);
        nameText = itemView.findViewById(R.id.text_message_name);
        profileImage = itemView.findViewById(R.id.image_message_profile);
    }

    public void bind(Message message, ChatActivity activity) {
        messageText.setText(message.getMsg());
        nameText.setText(message.getSender());
        ImageUtils.displayRoundImageFromUrl(activity,
                message.getUrlProfileImage(), profileImage);
        timeText.setText(TimeUtils.formatTime(message.getCreatedAt()));
    }
}

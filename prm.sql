/*
 Navicat Premium Data Transfer

 Source Server         : MySQL
 Source Server Type    : MySQL
 Source Server Version : 80011
 Source Host           : localhost:3306
 Source Schema         : prm

 Target Server Type    : MySQL
 Target Server Version : 80011
 File Encoding         : 65001

 Date: 11/07/2018 21:39:26
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for dom
-- ----------------------------
DROP TABLE IF EXISTS `dom`;
CREATE TABLE `dom`  (
  `dId` int(11) NOT NULL AUTO_INCREMENT,
  `dName` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`dId`) USING BTREE,
  INDEX `dId`(`dId`) USING BTREE,
  INDEX `dId_2`(`dId`) USING BTREE,
  INDEX `dId_3`(`dId`) USING BTREE,
  INDEX `dId_4`(`dId`) USING BTREE,
  INDEX `dId_5`(`dId`) USING BTREE,
  INDEX `dId_6`(`dId`) USING BTREE,
  INDEX `dId_7`(`dId`) USING BTREE,
  INDEX `dId_8`(`dId`) USING BTREE,
  INDEX `dId_9`(`dId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dom
-- ----------------------------
INSERT INTO `dom` VALUES (1, 'A');
INSERT INTO `dom` VALUES (2, 'B');
INSERT INTO `dom` VALUES (3, 'C');
INSERT INTO `dom` VALUES (4, 'D');
INSERT INTO `dom` VALUES (5, 'E');

-- ----------------------------
-- Table structure for major subject
-- ----------------------------
DROP TABLE IF EXISTS `major subject`;
CREATE TABLE `major subject`  (
  `mId` int(11) NOT NULL AUTO_INCREMENT,
  `mName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`mId`) USING BTREE,
  INDEX `mId`(`mId`) USING BTREE,
  INDEX `mId_2`(`mId`) USING BTREE,
  INDEX `mId_3`(`mId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of major subject
-- ----------------------------
INSERT INTO `major subject` VALUES (1, 'Software Engineering');
INSERT INTO `major subject` VALUES (2, 'Linguistics');
INSERT INTO `major subject` VALUES (3, 'Economics');

-- ----------------------------
-- Table structure for room
-- ----------------------------
DROP TABLE IF EXISTS `room`;
CREATE TABLE `room`  (
  `rId` int(11) NOT NULL AUTO_INCREMENT,
  `dId` int(255) NULL DEFAULT NULL,
  `rName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `slot` int(255) NULL DEFAULT NULL,
  PRIMARY KEY (`rId`) USING BTREE,
  INDEX `dId`(`dId`) USING BTREE,
  INDEX `rId`(`rId`) USING BTREE,
  INDEX `rId_2`(`rId`) USING BTREE,
  INDEX `rId_3`(`rId`) USING BTREE,
  INDEX `rId_4`(`rId`) USING BTREE,
  INDEX `rId_5`(`rId`) USING BTREE,
  INDEX `rId_6`(`rId`) USING BTREE,
  INDEX `rId_7`(`rId`) USING BTREE,
  CONSTRAINT `room_ibfk_1` FOREIGN KEY (`dId`) REFERENCES `dom` (`did`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of room
-- ----------------------------
INSERT INTO `room` VALUES (1, 4, '104', 6);
INSERT INTO `room` VALUES (2, 4, '101', 4);
INSERT INTO `room` VALUES (3, 4, '202', 4);
INSERT INTO `room` VALUES (4, 4, '204', 4);
INSERT INTO `room` VALUES (5, 1, '401', 6);
INSERT INTO `room` VALUES (6, 3, '103', 6);
INSERT INTO `room` VALUES (7, 3, '101', 6);
INSERT INTO `room` VALUES (8, 3, '201', 4);
INSERT INTO `room` VALUES (9, 3, '403', 4);
INSERT INTO `room` VALUES (10, 1, '102', 4);
INSERT INTO `room` VALUES (11, 3, '102', 6);
INSERT INTO `room` VALUES (12, 3, '202', 6);
INSERT INTO `room` VALUES (13, 5, '402', 4);
INSERT INTO `room` VALUES (14, 5, '401', 6);
INSERT INTO `room` VALUES (15, 5, '202', 4);
INSERT INTO `room` VALUES (16, 3, '203', 6);
INSERT INTO `room` VALUES (17, 4, '201', 6);
INSERT INTO `room` VALUES (18, 1, '103', 6);
INSERT INTO `room` VALUES (19, 2, '201', 6);
INSERT INTO `room` VALUES (20, 3, '301', 6);

-- ----------------------------
-- Table structure for student
-- ----------------------------
DROP TABLE IF EXISTS `student`;
CREATE TABLE `student`  (
  `sId` int(11) NOT NULL AUTO_INCREMENT,
  `fullName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `rollNumber` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `gender` tinyint(1) NOT NULL,
  `dateOfBirth` datetime(0) NOT NULL,
  `room` int(255) NOT NULL,
  `majorID` int(11) NOT NULL,
  PRIMARY KEY (`sId`) USING BTREE,
  INDEX `room`(`room`) USING BTREE,
  INDEX `majorID`(`majorID`) USING BTREE,
  CONSTRAINT `student_ibfk_2` FOREIGN KEY (`room`) REFERENCES `room` (`rid`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `student_ibfk_3` FOREIGN KEY (`majorID`) REFERENCES `major subject` (`mid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of student
-- ----------------------------
INSERT INTO `student` VALUES (1, 'Bao Nguyen', 'SE04950', 1, '1995-03-06 21:23:26', 1, 1);
INSERT INTO `student` VALUES (2, 'Lucas Elliott', 'SE01000', 0, '1996-06-02 00:00:00', 15, 2);
INSERT INTO `student` VALUES (3, 'Patricia Colon', 'SE01013', 0, '1995-10-01 00:00:00', 9, 2);
INSERT INTO `student` VALUES (4, 'Xander Contreras', 'SE01026', 0, '1995-10-29 00:00:00', 8, 3);
INSERT INTO `student` VALUES (5, 'Eden Bond', 'SE01039', 0, '1996-12-13 00:00:00', 9, 1);
INSERT INTO `student` VALUES (6, 'Idola Wade', 'SE01052', 1, '1996-10-06 00:00:00', 19, 3);
INSERT INTO `student` VALUES (7, 'Octavius Roberts', 'SE01065', 1, '1996-12-06 00:00:00', 1, 1);
INSERT INTO `student` VALUES (8, 'Wanda Wyatt', 'SE01078', 0, '1996-07-22 00:00:00', 8, 3);
INSERT INTO `student` VALUES (9, 'Bert Mays', 'SE01091', 1, '1995-10-11 00:00:00', 3, 3);
INSERT INTO `student` VALUES (10, 'Lara Tate', 'SE01104', 1, '1996-11-26 00:00:00', 17, 2);
INSERT INTO `student` VALUES (11, 'Laura Alexander', 'SE01117', 1, '1996-07-12 00:00:00', 6, 2);
INSERT INTO `student` VALUES (12, 'Igor Garrett', 'SE01130', 1, '1997-07-16 00:00:00', 15, 2);
INSERT INTO `student` VALUES (13, 'Lunea Branch', 'SE01143', 1, '1996-12-18 00:00:00', 1, 2);
INSERT INTO `student` VALUES (14, 'Alana Bradford', 'SE01156', 1, '1996-06-03 00:00:00', 12, 2);
INSERT INTO `student` VALUES (15, 'Beau Good', 'SE01169', 1, '1996-12-23 00:00:00', 2, 3);
INSERT INTO `student` VALUES (16, 'Quinlan Watkins', 'SE01182', 1, '1996-09-30 00:00:00', 10, 2);
INSERT INTO `student` VALUES (17, 'Ahmed William', 'SE01195', 0, '1997-04-24 00:00:00', 2, 2);
INSERT INTO `student` VALUES (18, 'Bruno Greer', 'SE01208', 0, '1997-02-03 00:00:00', 18, 2);
INSERT INTO `student` VALUES (19, 'Sebastian Slater', 'SE01221', 1, '1997-03-21 00:00:00', 15, 2);
INSERT INTO `student` VALUES (20, 'Maggy Olsen', 'SE01234', 0, '1995-12-23 00:00:00', 19, 2);
INSERT INTO `student` VALUES (21, 'Kelsie Ramos', 'SE01247', 0, '1997-02-13 00:00:00', 3, 3);
INSERT INTO `student` VALUES (22, 'Amos Calderon', 'SE01260', 1, '1996-06-15 00:00:00', 11, 2);
INSERT INTO `student` VALUES (23, 'Carson Bray', 'SE01273', 1, '1996-01-25 00:00:00', 13, 2);
INSERT INTO `student` VALUES (24, 'Ivory Shaw', 'SE01286', 1, '1997-01-26 00:00:00', 1, 3);
INSERT INTO `student` VALUES (25, 'Jaquelyn Sherman', 'SE01299', 1, '1997-06-30 00:00:00', 16, 1);
INSERT INTO `student` VALUES (26, 'Nomlanga Alvarado', 'SE01312', 1, '1995-08-05 00:00:00', 5, 1);
INSERT INTO `student` VALUES (27, 'Jacqueline Watson', 'SE01325', 1, '1997-01-23 00:00:00', 17, 2);
INSERT INTO `student` VALUES (28, 'MacKensie Brock', 'SE01338', 0, '1995-12-12 00:00:00', 9, 3);
INSERT INTO `student` VALUES (29, 'Kyra Dillon', 'SE01351', 0, '1995-12-12 00:00:00', 6, 2);
INSERT INTO `student` VALUES (30, 'Carl Carr', 'SE01364', 1, '1996-11-27 00:00:00', 12, 3);
INSERT INTO `student` VALUES (31, 'Genevieve Mcfadden', 'SE01377', 0, '1995-07-20 00:00:00', 7, 1);
INSERT INTO `student` VALUES (32, 'Yvonne Roach', 'SE01390', 1, '1997-03-01 00:00:00', 4, 3);
INSERT INTO `student` VALUES (33, 'Aaron Hodges', 'SE01403', 1, '1997-03-21 00:00:00', 10, 1);
INSERT INTO `student` VALUES (34, 'Paki Weber', 'SE01416', 0, '1995-11-02 00:00:00', 14, 2);
INSERT INTO `student` VALUES (35, 'Nomlanga Rosario', 'SE01429', 1, '1996-03-16 00:00:00', 7, 1);
INSERT INTO `student` VALUES (36, 'Zelenia Decker', 'SE01442', 0, '1996-07-22 00:00:00', 10, 1);
INSERT INTO `student` VALUES (37, 'Damon Curry', 'SE01455', 0, '1996-07-21 00:00:00', 20, 2);
INSERT INTO `student` VALUES (38, 'William Jackson', 'SE01468', 0, '1995-10-16 00:00:00', 18, 2);
INSERT INTO `student` VALUES (39, 'Jamal Velez', 'SE01481', 0, '1996-05-23 00:00:00', 20, 2);
INSERT INTO `student` VALUES (40, 'Ira Farmer', 'SE01494', 1, '1996-08-24 00:00:00', 10, 2);
INSERT INTO `student` VALUES (41, 'Ishmael Newton', 'SE01507', 1, '1997-01-12 00:00:00', 12, 2);
INSERT INTO `student` VALUES (42, 'Bo Fuentes', 'SE01520', 0, '1996-10-28 00:00:00', 4, 1);
INSERT INTO `student` VALUES (43, 'Dorothy Barron', 'SE01533', 0, '1996-06-11 00:00:00', 18, 3);
INSERT INTO `student` VALUES (44, 'Nathan Phillips', 'SE01546', 0, '1996-10-05 00:00:00', 15, 3);
INSERT INTO `student` VALUES (45, 'Margaret Mcdaniel', 'SE01559', 0, '1996-07-28 00:00:00', 1, 2);
INSERT INTO `student` VALUES (46, 'Shaine Nielsen', 'SE01572', 1, '1997-06-24 00:00:00', 2, 3);
INSERT INTO `student` VALUES (47, 'Vivien Rich', 'SE01585', 0, '1996-04-04 00:00:00', 14, 3);
INSERT INTO `student` VALUES (48, 'Flavia Ray', 'SE01598', 1, '1997-06-12 00:00:00', 4, 2);
INSERT INTO `student` VALUES (49, 'Kuame Head', 'SE01611', 0, '1996-10-29 00:00:00', 16, 3);
INSERT INTO `student` VALUES (50, 'Duncan Melton', 'SE01624', 0, '1996-12-13 00:00:00', 18, 3);
INSERT INTO `student` VALUES (51, 'Hashim Roy', 'SE01637', 0, '1995-09-06 00:00:00', 11, 3);
INSERT INTO `student` VALUES (52, 'Allen Harrington', 'SE01650', 0, '1997-06-17 00:00:00', 1, 1);
INSERT INTO `student` VALUES (53, 'Nomlanga Burton', 'SE01663', 1, '1995-07-22 00:00:00', 13, 2);
INSERT INTO `student` VALUES (54, 'Byron Yang', 'SE01676', 0, '1996-05-13 00:00:00', 1, 2);
INSERT INTO `student` VALUES (55, 'Macey Saunders', 'SE01689', 0, '1995-10-27 00:00:00', 14, 3);
INSERT INTO `student` VALUES (56, 'Alma Casey', 'SE01702', 1, '1996-09-07 00:00:00', 10, 2);
INSERT INTO `student` VALUES (57, 'Herrod Mason', 'SE01715', 1, '1996-02-14 00:00:00', 15, 3);
INSERT INTO `student` VALUES (58, 'Morgan Meyer', 'SE01728', 0, '1996-02-09 00:00:00', 19, 3);
INSERT INTO `student` VALUES (59, 'Brendan Miller', 'SE01741', 1, '1995-12-04 00:00:00', 4, 1);
INSERT INTO `student` VALUES (60, 'Hilel Mcclain', 'SE01754', 1, '1997-01-12 00:00:00', 5, 3);
INSERT INTO `student` VALUES (61, 'Bethany Rogers', 'SE01767', 1, '1996-07-16 00:00:00', 13, 3);
INSERT INTO `student` VALUES (62, 'Ciara Kelley', 'SE01780', 1, '1997-04-05 00:00:00', 4, 2);
INSERT INTO `student` VALUES (63, 'Blythe Hampton', 'SE01793', 1, '1997-06-30 00:00:00', 5, 2);
INSERT INTO `student` VALUES (64, 'Imogene Coffey', 'SE01806', 1, '1997-06-20 00:00:00', 3, 3);
INSERT INTO `student` VALUES (65, 'Michelle Daniels', 'SE01819', 1, '1997-07-25 00:00:00', 19, 1);
INSERT INTO `student` VALUES (66, 'Merrill Hatfield', 'SE01832', 0, '1996-11-27 00:00:00', 3, 2);
INSERT INTO `student` VALUES (67, 'Germaine Fleming', 'SE01845', 1, '1996-03-06 00:00:00', 6, 1);
INSERT INTO `student` VALUES (68, 'Stewart Shields', 'SE01858', 1, '1996-04-10 00:00:00', 15, 2);
INSERT INTO `student` VALUES (69, 'Olivia Sanchez', 'SE01871', 1, '1997-02-02 00:00:00', 9, 1);
INSERT INTO `student` VALUES (70, 'Dante Hicks', 'SE01884', 1, '1996-08-22 00:00:00', 20, 3);
INSERT INTO `student` VALUES (71, 'Brandon Hopper', 'SE01897', 1, '1996-11-13 00:00:00', 16, 3);
INSERT INTO `student` VALUES (72, 'Duncan Pittman', 'SE01910', 0, '1996-07-03 00:00:00', 12, 3);
INSERT INTO `student` VALUES (73, 'Scarlet Booker', 'SE01923', 0, '1996-07-02 00:00:00', 2, 1);
INSERT INTO `student` VALUES (74, 'Lenore Avila', 'SE01936', 0, '1995-11-23 00:00:00', 2, 1);
INSERT INTO `student` VALUES (75, 'Callie Justice', 'SE01949', 1, '1995-11-13 00:00:00', 18, 2);
INSERT INTO `student` VALUES (76, 'Kenyon Dawson', 'SE01962', 1, '1996-02-06 00:00:00', 16, 2);
INSERT INTO `student` VALUES (77, 'Sybil Foreman', 'SE01975', 1, '1997-05-29 00:00:00', 3, 3);
INSERT INTO `student` VALUES (78, 'Devin Barrett', 'SE01988', 0, '1995-11-18 00:00:00', 5, 3);
INSERT INTO `student` VALUES (79, 'Kuame Burgess', 'SE02001', 1, '1996-03-12 00:00:00', 6, 1);
INSERT INTO `student` VALUES (80, 'Cyrus Goodman', 'SE02014', 1, '1997-07-21 00:00:00', 8, 3);
INSERT INTO `student` VALUES (81, 'Nash Bradshaw', 'SE02027', 1, '1995-08-24 00:00:00', 12, 2);
INSERT INTO `student` VALUES (82, 'Beau Glover', 'SE02040', 0, '1996-07-11 00:00:00', 5, 2);
INSERT INTO `student` VALUES (83, 'Giacomo Hale', 'SE02053', 1, '1996-12-01 00:00:00', 20, 3);
INSERT INTO `student` VALUES (84, 'Jin Bender', 'SE02066', 0, '1995-12-17 00:00:00', 10, 3);
INSERT INTO `student` VALUES (85, 'Len Cole', 'SE02079', 0, '1996-07-07 00:00:00', 18, 1);
INSERT INTO `student` VALUES (86, 'Jenna Vincent', 'SE02092', 1, '1997-04-05 00:00:00', 5, 3);
INSERT INTO `student` VALUES (87, 'Burton Rocha', 'SE02105', 0, '1996-08-10 00:00:00', 17, 3);
INSERT INTO `student` VALUES (88, 'Rinah Newton', 'SE02118', 1, '1996-10-16 00:00:00', 6, 1);
INSERT INTO `student` VALUES (89, 'Richard Mooney', 'SE02131', 1, '1996-09-01 00:00:00', 10, 1);
INSERT INTO `student` VALUES (90, 'Abraham Herrera', 'SE02144', 1, '1995-11-04 00:00:00', 4, 1);
INSERT INTO `student` VALUES (91, 'Amela Mayer', 'SE02157', 1, '1997-06-12 00:00:00', 2, 2);
INSERT INTO `student` VALUES (92, 'Skyler Mcgee', 'SE02170', 0, '1997-05-01 00:00:00', 15, 3);
INSERT INTO `student` VALUES (93, 'Alec Noel', 'SE02183', 0, '1997-05-08 00:00:00', 18, 3);
INSERT INTO `student` VALUES (94, 'Lillian Pratt', 'SE02196', 0, '1996-10-18 00:00:00', 18, 2);
INSERT INTO `student` VALUES (95, 'Henry Reid', 'SE02209', 1, '1996-10-25 00:00:00', 18, 1);
INSERT INTO `student` VALUES (96, 'Jolene Beasley', 'SE02222', 1, '1997-02-15 00:00:00', 8, 1);
INSERT INTO `student` VALUES (97, 'Lavinia Taylor', 'SE02235', 1, '1995-12-17 00:00:00', 16, 2);
INSERT INTO `student` VALUES (98, 'Daquan Harper', 'SE02248', 0, '1996-06-08 00:00:00', 3, 3);
INSERT INTO `student` VALUES (99, 'Chava Mcbride', 'SE02261', 0, '1997-03-11 00:00:00', 20, 3);
INSERT INTO `student` VALUES (100, 'Addison Kramer', 'SE02274', 0, '1997-04-27 00:00:00', 5, 3);
INSERT INTO `student` VALUES (101, 'Calvin Sweeney', 'SE02287', 1, '1995-12-20 00:00:00', 7, 2);

SET FOREIGN_KEY_CHECKS = 1;
